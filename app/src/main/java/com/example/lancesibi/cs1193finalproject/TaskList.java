package com.example.lancesibi.cs1193finalproject;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lancesibi on 11/28/2017.
 */

public class TaskList extends RealmObject {
    @PrimaryKey
    String id;

    String name;
    RealmList<Item> tasks;
    @LinkingObjects("lists")
    private final RealmResults<User> users;

    public TaskList()
    {
        tasks = new RealmList<>();
        users = null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<Item> getTasks() {
        return tasks;
    }

    public void setTasks(RealmList<Item> tasks) {
        this.tasks = tasks;
    }
}

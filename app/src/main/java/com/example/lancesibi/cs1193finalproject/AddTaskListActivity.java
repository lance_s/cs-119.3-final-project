package com.example.lancesibi.cs1193finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.UUID;

import io.realm.Realm;

@EActivity(R.layout.activity_add_task_list)
public class AddTaskListActivity extends AppCompatActivity {

    @ViewById(R.id.editText)
    EditText listName;

    String loggedInUsername;

    @AfterViews
    public void init() {
        Intent intent = getIntent();
        loggedInUsername = intent.getStringExtra("user2");
    }

    @Click(R.id.savebtn)
    public void saveList()
    {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        User user = realm.where(User.class).equalTo("username", loggedInUsername).findFirst();
        TaskList newTaskList = new TaskList();
        newTaskList.setId(UUID.randomUUID().toString());
        newTaskList.setName(listName.getText().toString());
        user.getLists().add(newTaskList);
        realm.commitTransaction();

        setResult(0);
        finish();
    }
}

package com.example.lancesibi.cs1193finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.UUID;

import io.realm.Realm;

@EActivity(R.layout.activity_additemform)
public class Additemform extends AppCompatActivity {
    @ViewById(R.id.AddTaskName)
    TextView tn;
    @ViewById(R.id.SaveAdd)
    Button Add;
    @ViewById(R.id.HighAdd)
    RadioButton high;
    @ViewById(R.id.MedAdd)
    RadioButton med;
    @ViewById(R.id.LowAdd)
    RadioButton low;
    @ViewById(R.id.NoneAdd)
    RadioButton none;
    Realm realm;
    String name;
    String listID;
    int priority;
    @AfterViews
    public void init()
    {
        realm = Realm.getDefaultInstance();

        Intent i = getIntent();
        listID = i.getStringExtra("listIDtoAssign");
    }

    @Click(R.id.SaveAdd)
    public void SaveAddedInput()
    {
//        something = tn.getText().toString();
        name = tn.getText().toString();
        if(high.isChecked())
        {
            priority = 3;
        }
        else if(med.isChecked())
        {
            priority = 2;
        }
        else if(low.isChecked())
        {
            priority = 1;
        }
        else if(none.isChecked())
        {
            priority = 0;
        }
        realm.beginTransaction();
        TaskList assignedList = realm.where(TaskList.class).equalTo("id", listID).findFirst();
        Item item = new Item();
        item.setName(name);
        item.setPriority(priority);
        item.setId(UUID.randomUUID().toString());
        assignedList.getTasks().add(item);
        realm.commitTransaction();
        finish();
    }

    @Override
    public void finish()
    {
        super.finish();
        realm.close();
    }
}

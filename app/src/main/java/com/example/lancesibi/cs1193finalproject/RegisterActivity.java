package com.example.lancesibi.cs1193finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import io.realm.Realm;

@EActivity(R.layout.activity_register)
public class RegisterActivity extends AppCompatActivity {

    @ViewById(R.id.etUser)
    EditText newName;

    @ViewById(R.id.etPass)
    EditText newPass;

    @Click(R.id.button)
    public void clickOK() {
        if (newName.getText().toString().equals("") || newPass.getText().toString().equals("")) {
            Toast.makeText(RegisterActivity.this, "Don't leave anything blank.", Toast.LENGTH_SHORT).show();
        } else {
            String username = newName.getText().toString();
            String password = newPass.getText().toString();

            Realm realm = Realm.getDefaultInstance();

            User tempuser = realm.where(User.class).equalTo("username", username).findFirst();

            if (tempuser != null) //if user already exists
                Toast.makeText(RegisterActivity.this, "Username already exists.", Toast.LENGTH_SHORT).show();
            else {
                register(username, password);
            }
        }
    }

    public void register(String username, String password) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();

        User newuser = new User();
        newuser.setUsername(username);
        newuser.setPassword(password);

        realm.copyToRealm(newuser);

        realm.commitTransaction();

        Intent i = new Intent();
        setResult(1, i);
        finish();
    }
}

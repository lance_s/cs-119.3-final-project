package com.example.lancesibi.cs1193finalproject;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lancesibi on 11/28/2017.
 */

public class User extends RealmObject {
    @PrimaryKey
    String username;

    String password;
    RealmList<TaskList> lists;

    public User()
    {
        lists = new RealmList<>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RealmList<TaskList> getLists() {
        return lists;
    }

    public void setLists(RealmList<TaskList> lists) {
        this.lists = lists;
    }
}

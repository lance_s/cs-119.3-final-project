package com.example.lancesibi.cs1193finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import javax.xml.datatype.Duration;

import io.realm.Realm;

@EActivity(R.layout.activity_edit_task_list)
public class EditTaskListActivity extends AppCompatActivity {


    @ViewById(R.id.editText2)
    EditText listName;

    String loggedInUsername;
    String passedID;
    String passedTaskListName;

    TaskList newTaskList;

    @AfterViews
    public void init() {
        Intent intent = getIntent();
        loggedInUsername = intent.getStringExtra("user3");
        passedID = intent.getStringExtra("taskListIDtoEdit");
        passedTaskListName = intent.getStringExtra("taskListNametoEdit");
        listName.setText(passedTaskListName);
    }

    @Click(R.id.savebtn)
    public void saveList()
    {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();

        newTaskList = realm.where(TaskList.class).equalTo("id", passedID).findFirst();

        if (newTaskList != null)
        {
            newTaskList.setName(listName.getText().toString());

            realm.commitTransaction();

            setResult(0);
            finish();
        }
        else
        {
            Toast.makeText(this, "idk", Toast.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.deletebtn)
    public void deleteList()
    {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();

        newTaskList = realm.where(TaskList.class).equalTo("id", passedID).findFirst();
        if (newTaskList != null)
            newTaskList.deleteFromRealm();
        else
            Toast.makeText(this, "idk", Toast.LENGTH_SHORT).show();
        realm.commitTransaction();

        setResult(1);
        finish();
    }
}

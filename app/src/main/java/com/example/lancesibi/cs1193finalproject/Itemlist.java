package com.example.lancesibi.cs1193finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmBaseAdapter;
import io.realm.RealmQuery;
import io.realm.RealmResults;

@EActivity(R.layout.activity_itemlist)
public class Itemlist extends AppCompatActivity {

    @ViewById(R.id.FirstAddBtn)
    Button FirstAddBtn;

    @ViewById(R.id.ListView)
    ListView lv;

    MyAdapter adapter;

    String listID;

    @AfterViews
    public void init()
    {
        Realm realm = Realm.getDefaultInstance();

        Intent intent = getIntent();
        listID = intent.getStringExtra("enter");

        RealmQuery<Item> query2 = realm.where(Item.class).equalTo("lists.id", listID);
        RealmResults<Item> res = query2.findAll();
        adapter = new MyAdapter(res);
        lv.setAdapter(adapter);
    }

    public class MyAdapter extends RealmBaseAdapter<Item>
    {
        public MyAdapter(OrderedRealmCollection<Item> realmResults)
        {
            super(realmResults);
        }

        public View getView(int position, View convertView, ViewGroup present)
        {
            Item i = adapterData.get(position);

            View v = null;
            if(convertView == null)
            {
                v = getLayoutInflater().inflate(R.layout.itemrow, null);
                System.out.println("convertView is null");
            }
            else
            {
                System.out.println("error here somewhere");
                v = convertView;
            }

            final TextView itemname = (TextView) v.findViewById(R.id.ItemTextView);
            String TaskName = "";

            if (i.isChecked()) {
                TaskName += "✓ ";
            }

            if (i.priority == 3)
            {
                TaskName += "!!! ";
            }
            else if (i.priority == 2)
            {
                TaskName += "!! ";
            }
            else if (i.priority == 1)
            {
                TaskName += "! ";
            }
            TaskName += i.getName();
            itemname.setText(TaskName);

            Button checkBtn = v.findViewById(R.id.CheckItem);
            checkBtn.setTag(i);
            checkBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button TheButton = (Button) v;
                    Item TheItem = (Item) TheButton.getTag();
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    if (!TheItem.isChecked())
                    {
                        String wa = "✓ " + TheItem.getName();
                        itemname.setText(wa);
                        TheItem.setChecked(true);
                    }
                    else if (TheItem.isChecked())
                    {
                        itemname.setText(TheItem.getName());
                        TheItem.setChecked(false);
                    }
                    realm.commitTransaction();
                }
            });

            Button bee = v.findViewById(R.id.EditItem);
            bee.setTag(i);
            bee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button TheButton = (Button) v;
                    Item TheItem = (Item) TheButton.getTag();
                    Edit(TheItem);
                }
            });
            return v;
        }
    }

    void Edit(Item i)
    {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
//        i.deleteFromRealm();
        Intent what = new Intent(this, Edititemform_.class);
        what.putExtra("priority", i.getPriority());
        what.putExtra("name", i.getName());
        what.putExtra("id", i.getId());
        realm.commitTransaction();
        startActivity(what);
    }

    @Click(R.id.FirstAddBtn)
    public void FirstAddThing()
    {
        Intent i = new Intent(this, Additemform_.class);

        i.putExtra("listIDtoAssign", listID);

        startActivityForResult(i, 0);
    }
}

package com.example.lancesibi.cs1193finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import io.realm.Realm;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.etUser)
    EditText inputName;

    @ViewById(R.id.etPass)
    EditText inputPass;

    @Click(R.id.btnLogin)
    public void login_click()
    {
        String inputUsername = inputName.getText().toString();
        String inputPassword = inputPass.getText().toString();

        Realm realm = Realm.getDefaultInstance();

        if (realm.where(User.class).count() == 0)
        {
            //Are there any users?
            Toast.makeText(MainActivity.this, "No users exist.", Toast.LENGTH_SHORT).show();
        }
        else if (inputUsername.equals("") || inputPassword.equals(""))
        {
            //Are there any blank spaces?
            Toast.makeText(MainActivity.this, "Please enter your credentials.", Toast.LENGTH_SHORT).show();
        }
        else
        {
            User searchedUser = realm.where(User.class).equalTo("username", inputUsername).findFirst();

            if (searchedUser != null) {
                String searchedName = searchedUser.getUsername();
                String searchedPass = searchedUser.getPassword();

                boolean correctCredentials = (searchedName.equals(inputUsername) && searchedPass.equals(inputPassword));

                if (correctCredentials) {
                    goToWelcomeScreen(searchedName);
                }
                else
                {
                    Toast.makeText(MainActivity.this, "Wrong credentials.", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Toast.makeText(MainActivity.this, "User not found.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void goToWelcomeScreen(String n)
    {
        Intent intent = new Intent(this, ShowTaskListsActivity_.class);

        intent.putExtra("user", n);

        startActivity(intent);

//        startActivityForResult(intent,0);
    }

    @Click(R.id.btnUser)
    public void goToRegister()
    {
        Intent i = new Intent(this, RegisterActivity_.class);
        int requestCode = 0;
        startActivityForResult(i, requestCode);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent i)
    {
        Toast.makeText(MainActivity.this, "Successfully registered.", Toast.LENGTH_SHORT).show();
    }
}

package com.example.lancesibi.cs1193finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.UUID;

import io.realm.Realm;

@EActivity(R.layout.activity_edititemform)
public class Edititemform extends AppCompatActivity {
    @ViewById(R.id.SaveEdit)
    Button Save;
    @ViewById(R.id.DeleteEdit)
    Button DeleteEditButton;
    @ViewById(R.id.EditedName)
    TextView en;
    @ViewById(R.id.HighEdit)
    RadioButton high;
    @ViewById(R.id.MedEdit)
    RadioButton med;
    @ViewById(R.id.LowEdit)
    RadioButton low;
    @ViewById(R.id.NoneEdit)
    RadioButton none;
    @ViewById(R.id.DeleteEdit)
    Button delete;
    Realm realm;
    String name;
    int priority;
    String AName;
    String AId;
    int APriority;
    Item TheItem;
    @AfterViews
    public  void init()
    {
        realm = Realm.getDefaultInstance();
        Intent i = getIntent();
        AName = i.getStringExtra("name");
        AId = i.getStringExtra("id");
        APriority = i.getIntExtra("priority", 0);
        en.setText(AName);

        if (APriority == 3) {
            high.setChecked(true);
        }
        else if (APriority == 2) {
            med.setChecked(true);
        }
        else if (APriority == 1) {
            low.setChecked(true);
        }
        else if (APriority == 0) {
            none.setChecked(true);
        }
    }
    @Click(R.id.SaveEdit)
    public void savechanges()
    {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        TheItem = realm.where(Item.class).equalTo("id", AId).findFirst();
        if(TheItem == null)
        {
            Toast.makeText(this, "Something is wrong here???", Toast.LENGTH_SHORT).show();
        }
        else
        {
            name = en.getText().toString();
            if(high.isChecked())
            {
                priority = 3;
            }
            else if (med.isChecked())
            {
                priority = 2;
            }
            else if (low.isChecked())
            {
                priority = 1;
            }
            else if (none.isChecked())
            {
                priority = 0;
            }

            TheItem.setName(name);
            TheItem.setPriority(priority);
            realm.commitTransaction();
            finish();
        }
    }

    @Click(R.id.DeleteEdit)
    public void close()
    {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        TheItem = realm.where(Item.class).equalTo("id", AId).findFirst();
        if(AId == null)
        {
            Toast.makeText(this, "Something is wrong here", Toast.LENGTH_SHORT).show();
        }
        else
        {
            TheItem.deleteFromRealm();
            realm.commitTransaction();
            finish();
        }


    }

    @Override
    public void finish()
    {
        super.finish();
        realm.close();
    }
}

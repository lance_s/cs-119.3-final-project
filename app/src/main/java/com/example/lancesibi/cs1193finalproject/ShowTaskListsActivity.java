package com.example.lancesibi.cs1193finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmBaseAdapter;
import io.realm.RealmQuery;
import io.realm.RealmResults;

@EActivity(R.layout.astl)
public class ShowTaskListsActivity extends AppCompatActivity {

    @ViewById(R.id.listView)
    ListView lv;

    String loggedInUsername;

    @AfterViews
    public void init() {
        Realm realm = Realm.getDefaultInstance();

        Intent intent = getIntent();
        loggedInUsername = intent.getStringExtra("user");

        RealmQuery<TaskList> q2 = realm.where(TaskList.class).equalTo("users.username", loggedInUsername);
        RealmResults<TaskList> r2 = q2.findAll();

        MyAdapter adapter = new MyAdapter(r2);
        lv.setAdapter(adapter);

    }

    class MyAdapter extends RealmBaseAdapter<TaskList> {

        public MyAdapter(OrderedRealmCollection<TaskList> realmResults) {
            super(realmResults);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            TaskList d = adapterData.get(position);

            View v = null;
            if (convertView == null) {
                System.out.println("CREATED ROW");
                v = getLayoutInflater().inflate(R.layout.row, null);
            } else {
                System.out.println("RECYCLED ROW");
                v = convertView;
            }

            //naming variables
            TextView name = (TextView) v.findViewById(R.id.textView4);

            //set data of the row
            name.setText(d.getName());

            //go to list
            Button goB = (Button) v.findViewById(R.id.gobutton);
            goB.setTag(d);

            goB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Button thebutton = (Button) view;

                    TaskList myData = (TaskList) thebutton.getTag();
                    gotoTaskList(myData);
                }
            });

            //edit
            Button editB = (Button) v.findViewById(R.id.button2);
            editB.setTag(d);
            editB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Button thebutton = (Button) view;

                    TaskList myData = (TaskList) thebutton.getTag();
                    edit(myData);
                }
            });


            return v;
        }
    }

    @Click(R.id.button)
    public void goToNewListScreen() {
        Intent intent = new Intent(this, AddTaskListActivity_.class);

        intent.putExtra("user2", loggedInUsername);

        startActivityForResult(intent, 0);
    }

    public void gotoTaskList(TaskList tl)
    {
        Intent intent = new Intent(this, Itemlist_.class);

        intent.putExtra("enter", tl.getId());

        startActivity(intent);
    }

    public void edit(TaskList tl)
    {
        Intent intent = new Intent(this, EditTaskListActivity_.class);

        intent.putExtra("user3", loggedInUsername);
        intent.putExtra("taskListIDtoEdit", tl.getId());
        intent.putExtra("taskListNametoEdit", tl.getName());
        startActivityForResult(intent, 1);
    }

    public void onActivityResult (int requestCode, int resultCode, Intent i)
    {
        if(requestCode == 0)
        {
            Toast.makeText(this, "New list created", Toast.LENGTH_SHORT).show();
        }
        else if (requestCode == 1)
        {
           if (resultCode == 0)
               Toast.makeText(this, "List updated", Toast.LENGTH_SHORT).show();
           else if (resultCode == 1) {
               Toast.makeText(this, "List deleted", Toast.LENGTH_SHORT).show();
           }
        }
    }
}
